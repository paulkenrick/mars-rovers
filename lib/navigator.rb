class Navigator
  attr_reader :plateau_x, :plateau_y, :rovers

  def initialize
    @plateau_x = nil
    @plateau_y = nil
    @rovers = []
  end

  def receive_instructions(instructions)
    commands = instructions.split("\n")

    plateau_size = commands.shift
    set_plateau(plateau_size)

    commands.each_slice(2) do |command_set|
      rover_position_string = command_set.first
      rover_movements_string = command_set.last

      rover = position_rover(rover_position_string)
      move_rover(rover, rover_movements_string)
    end

    rover_positions
  end

  def set_plateau(plateau_size_string)
    plateau_size = plateau_size_string.split(' ')
    @plateau_x = plateau_size.first.to_i
    @plateau_y = plateau_size.last.to_i
  end

  def position_rover(rover_position_string)
    rover_position = rover_position_string.split(' ')
    rover = Rover.new(rover_position[0].to_i, rover_position[1].to_i, rover_position[2])
    @rovers << rover
    rover
  end

  def move_rover(rover, rover_movements_string)
    rover_movements_string.split("").each do |single_movement|
      rover.drive(single_movement)
    end
  end

  def rover_positions
    @rovers.each.map{ |rover| "#{rover.location_x} #{rover.location_y} #{rover.direction}" }.join("\n")
  end

end
