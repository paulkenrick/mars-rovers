class Rover
  attr_reader :direction, :location_x, :location_y

  POSSIBLE_DIRECTIONS = ['N', 'E', 'S', 'W']

  def initialize(location_x, location_y, direction)
    @location_x = location_x
    @location_y = location_y
    @direction = direction
  end

  def drive(movement)
    case movement
    when 'L'
      turn_left
    when 'R'
      turn_right
    when 'M'
      move
    end
  end

  def turn_left
    @direction = POSSIBLE_DIRECTIONS[POSSIBLE_DIRECTIONS.find_index(@direction) - 1]
  end

  def turn_right
    @direction = POSSIBLE_DIRECTIONS[(POSSIBLE_DIRECTIONS.find_index(@direction) + 1) % 4]
  end

  def move
    case @direction
    when 'N'
      @location_y += 1
    when 'E'
      @location_x += 1
    when 'S'
      @location_y -= 1
    when 'W'
      @location_x -= 1
    end
  end

end
