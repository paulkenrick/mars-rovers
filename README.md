# Mars Rover Coding Challenge

A squad of robotic rovers are to be landed by NASA on a plateau on Mars. This plateau, which is curiously rectangular, must be navigated by the rovers so that their on board cameras can get a complete view of the surrounding terrain to send back to Earth. A rover's position is represented by a combination of an x and y co-ordinates and a letter representing one of the four cardinal compass points.

The plateau is divided up into a grid to simplify navigation. An example position might be 0, 0, N, which means the rover is in the bottom left corner and facing North. In order to control a rover, NASA sends a simple string of letters. The possible letters are 'L', 'R' and 'M'. 'L' and 'R' makes the rover spin 90 degrees left or right respectively, without moving from its current spot. 'M' means move forward one grid point, and maintain the same heading. Assume that the square directly North from (x, y) is (x, y+1).

**Input:**

The first line of input is the upper-right coordinates of the plateau, the lower-left coordinates are assumed to be 0,0.
The rest of the input is information pertaining to the rovers that have been deployed. Each rover has two lines of input. The first line gives the rover's position, and the second line is a series of instructions telling the rover how to explore the plateau.

The position is made up of two integers and a letter separated by spaces, corresponding to the x and y co-ordinates and the rover's orientation. Each rover will be finished sequentially, which means that the second rover won't start to move until the first one has finished moving.

**Output:**

The output for each rover should be its final co-ordinates and heading.

## How I Built the Application

I built this application in ruby following the TDD process. I estimate this took about 3 - 4 hours.

I decided to break the application up into two classes having the following responsibilities:

1. **Navigator class** is responsible for interpreting instructions from the user and directing rovers based on these instructions (i.e. setting up the plateau where the rovers will operate, and placing and moving the rovers).

2. **Rover class** is responsible for simply turning/moving when told to by the navigator.

## Getting Started

To execute the application, include the input you want to process in the `input.txt` file (in the prescribed format), and then run `ruby execute.rb` from the root directory.

Note: ruby must be installed to run this application.

## Running the tests

RSpec unit tests have been included for all model methods. I have also included a short feature spec to easily test that the correct output is received from the application as a whole. All tests include hardcoded input in string format rather than rely on an input file, so that they don't have any external dependencies.

To run the feature test: `bundle exec rspec ./spec/feature_specs/navigation_result_spec.rb`

To run all tests: `bundle exec rspec`

Note: to run these tests you will need to have RSpec installed.  This is bundled into the application, so I recommend using Bundler to install it by running `bundle install` (you must have bundler installed to do this), otherwise install RSpec manually with `gem install rspec`.
