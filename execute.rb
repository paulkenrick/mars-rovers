require_relative "./lib/navigator"
require_relative "./lib/rover"

navigator = Navigator.new
rover_positions = navigator.receive_instructions(File.read('input.txt'))

puts rover_positions
