require "spec_helper"

RSpec.describe Navigator, type: :model do
  let(:navigator) { Navigator.new }

  describe '#receives_instructions' do
    let(:rover1) { Rover.new(1, 2, 'N') }
    let(:rover2) { Rover.new(3, 3, 'E') }

    after(:each) do
      navigator.receive_instructions("5 5\n1 2 N\nLMLMLMLMM\n3 3 E\nMMRMMRMRRM\n")
    end

    it 'calls #set_plateau with the correct arguments from input line 1' do
      plateau_size = "5 5"
      expect(navigator).to receive(:set_plateau).with(plateau_size)
    end

    it 'calls #position_rover with the correct arguments from input lines 2 and 4' do
      allow(navigator).to receive(:position_rover).with("1 2 N").and_return(rover1)
      allow(navigator).to receive(:position_rover).with("3 3 E").and_return(rover2)
      expect(navigator).to receive(:position_rover).once.with("1 2 N")
      expect(navigator).to receive(:position_rover).once.with("3 3 E")
    end

    it 'calls #move_rover with the correct arguments from input line 2' do
      allow(navigator).to receive(:position_rover).with("1 2 N").and_return(rover1)
      allow(navigator).to receive(:position_rover).with("3 3 E").and_return(rover2)
      expect(navigator).to receive(:move_rover).once.with(rover1, "LMLMLMLMM")
      expect(navigator).to receive(:move_rover).once.with(rover2, "MMRMMRMRRM")
    end

    it 'returns the result of #rover_positions' do
      expect(navigator).to receive(:receive_instructions).and_return("1 3 N\n5 1 E")
    end
  end

  describe '#set_plateau' do
    it 'sets instance variables correctly' do
      navigator.set_plateau("20 30")
      expect(navigator.plateau_x).to eq(20)
      expect(navigator.plateau_y).to eq(30)
    end
  end

  describe '#position_rover' do
    it 'creates a new rover with the correct position and direction and adds it to @rovers' do
      navigator.position_rover("1 2 N")
      expect(navigator.rovers.first).to have_attributes(location_x: 1, location_y: 2, direction: "N")
    end
  end

  describe '#move_rover' do
    it 'calls the rover\'s drive method on each single movement in the command provided' do
      rover = Rover.new(1, 2, 'N')
      expect(rover).to receive(:drive).with("L").exactly(4).times
      expect(rover).to receive(:drive).with("M").exactly(5).times
      navigator.move_rover(rover, "LMLMLMLMM")
    end
  end

end
