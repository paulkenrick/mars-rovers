require 'spec_helper'

RSpec.describe Rover, type: :model do

  describe '#turn_left' do
    describe 'when rover is pointing north' do
      it 'sets the rovers direction to be west' do
        rover = Rover.new(1, 2, 'N')
        rover.turn_left
        expect(rover.direction).to eq('W')
      end
    end

    describe 'when rover is pointing east' do
      it 'sets the rovers direction to be north' do
        rover = Rover.new(1, 2, 'E')
        rover.turn_left
        expect(rover.direction).to eq('N')
      end
    end

    describe 'when rover is pointing south' do
      it 'sets the rovers direction to be east' do
        rover = Rover.new(1, 2, 'S')
        rover.turn_left
        expect(rover.direction).to eq('E')
      end
    end

    describe 'when rover is pointing west' do
      it 'sets the rovers direction to be south' do
        rover = Rover.new(1, 2, 'W')
        rover.turn_left
        expect(rover.direction).to eq('S')
      end
    end
  end

  describe '#turn_right' do
    describe 'when rover is pointing north' do
      it 'sets the rovers direction to be east' do
        rover = Rover.new(1, 2, 'N')
        rover.turn_right
        expect(rover.direction).to eq('E')
      end
    end

    describe 'when rover is pointing east' do
      it 'sets the rovers direction to be south' do
        rover = Rover.new(1, 2, 'E')
        rover.turn_right
        expect(rover.direction).to eq('S')
      end
    end

    describe 'when rover is pointing south' do
      it 'sets the rovers direction to be west' do
        rover = Rover.new(1, 2, 'S')
        rover.turn_right
        expect(rover.direction).to eq('W')
      end
    end

    describe 'when rover is pointing west' do
      it 'sets the rovers direction to be north' do
        rover = Rover.new(1, 2, 'W')
        rover.turn_right
        expect(rover.direction).to eq('N')
      end
    end
  end

  describe '#move' do
    describe 'when rover is pointing north' do
      it 'moves the rover one unit upwards' do
        rover = Rover.new(1, 2, 'N')
        rover.move
        expect(rover.location_x).to eq(1)
        expect(rover.location_y).to eq(3)
      end
    end

    describe 'when rover is pointing east' do
      it 'moves the rover one unit to the right' do
        rover = Rover.new(1, 2, 'E')
        rover.move
        expect(rover.location_x).to eq(2)
        expect(rover.location_y).to eq(2)
      end
    end

    describe 'when rover is pointing south' do
      it 'moves the rover one unit downwards' do
        rover = Rover.new(1, 2, 'S')
        rover.move
        expect(rover.location_x).to eq(1)
        expect(rover.location_y).to eq(1)
      end
    end

    describe 'when rover is pointing west' do
      it 'moves the rover one unit to the left' do
        rover = Rover.new(1, 2, 'W')
        rover.move
        expect(rover.location_x).to eq(0)
        expect(rover.location_y).to eq(2)
      end
    end
  end

end
