require 'spec_helper'

RSpec.describe 'Navigation result', type: :feature do

  it 'outputs the correct rover destinations' do
    navigator = Navigator.new
    rover_positions = navigator.receive_instructions("5 5\n1 2 N\nLMLMLMLMM\n3 3 E\nMMRMMRMRRM\n")
    expect(rover_positions).to eq("1 3 N\n5 1 E")
  end

end
